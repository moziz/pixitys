import { Application, Assets, Point, Sprite, Ticker } from 'pixi.js';
import 'pixi.js/math-extras';

import './main.css';
import naama from './naama.png';

const app = new Application();
await app.init({ background: '#282723', resizeTo: window });

document.body.appendChild(app.canvas);

const naamaTex = await Assets.load(naama);

const naamaSprite = new Sprite(naamaTex);
naamaSprite.anchor.set(.5, .5);
naamaSprite.position = { x: 300, y: 300 };
naamaSprite.rotation = Math.PI / 2;

app.stage.addChild(naamaSprite);

let naamaHeading = new Point(1, 1);
const naamaSpeed = 10;

app.ticker.add((time: Ticker) => {
	console.log(time.deltaTime);

	const rotationSpeed = (Math.PI * 2);
	naamaSprite.rotation = time.lastTime / 1000 * rotationSpeed;

	const tweaky = () => (new Point(app.screen.width / 2, app.screen.height / 2).subtract(naamaSprite.position).normalize()).multiplyScalar((Math.random() * 0.025) * time.deltaTime);
	naamaHeading = naamaHeading.add(tweaky());
	naamaHeading = naamaHeading.normalize();

	naamaHeading.x *= naamaSprite.position.x > app.screen.right && naamaHeading.x > 0 ? -1 : 1;
	naamaHeading.x *= naamaSprite.position.x < app.screen.left && naamaHeading.x < 0 ? -1 : 1;
	naamaHeading.y *= naamaSprite.position.y < app.screen.top && naamaHeading.y < 0 ? -1 : 1;
	naamaHeading.y *= naamaSprite.position.y > app.screen.bottom && naamaHeading.y > 0 ? -1 : 1;

	const moveDelta = naamaHeading.multiplyScalar(naamaSpeed * time.deltaTime);

	naamaSprite.position = naamaSprite.position.add(moveDelta);
});


app.ticker.add((time: Ticker) => {

});